﻿using Nest;
using NUnit.Framework;
using System;
using System.Linq;
using ElasticSearch.Dtos;

namespace ElasticSearch
{
    public class BasicTestsWithNest
    {
        private ElasticClient client;
        private Employee[] employees;

        [SetUp]
        public void Setup()
        {
            var node = new Uri("http://localhost:9200");

            var settings = new ConnectionSettings(node);
            settings.DefaultIndex("megacorp");

            client = new ElasticClient(settings);

            client.DeleteIndex(Indices.Index("megacorp"));

            employees = new[]
            {
                new Employee
                {
                    FirstName = "John",
                    LastName = "Smith",
                    Age = 25,
                    About = "I love to go rock climbing",
                    Interests = new[] {"sports", "music"}
                },
                new Employee
                {
                    FirstName = "Jane",
                    LastName = "Smith",
                    Age = 32,
                    About = "I like to collect rock albums",
                    Interests = new[] {"music"}
                },
                new Employee
                {
                    FirstName = "Douglas",
                    LastName = "Fir",
                    Age = 35,
                    About = "I like to build cabinets",
                    Interests = new[] {"forestry"}
                }
            };
        }

        [Test]
        public void Can_insert_documents()
        {
            client.Index(employees[0]);

            var retrieved = client.Search<Employee>();
            Assert.AreEqual(employees[0].FirstName, retrieved.Documents.Single().FirstName);
        }

        [Test]
        public void Can_retrieve_document_by_index()
        {
            InsertAllEmployees();

            var retrieved = client.Get<Employee>(new GetRequest("megacorp", "employee", 1));
            Assert.AreEqual(employees[0].FirstName, retrieved.Source.FirstName);
        }

        [Test]
        public void Can_search_by_document_field()
        {
            InsertAllEmployees();

            var retrieved = 
                client.Search<Employee>(s => s.Query(q => q.Term(d => d.LastName, "Smith")))
                .Documents.OrderBy(d => d.FirstName).ToArray();
            Assert.AreEqual(2, retrieved.Length);
            Assert.AreEqual("Jane", retrieved[0].FirstName);
            Assert.AreEqual("John", retrieved[1].FirstName);
        }

        private void InsertAllEmployees()
        {
            for(int i=0; i<employees.Length; i++)
                client.Index(employees[i], idesc => idesc.Id(i + 1));
        }
    }
}
