﻿using NUnit.Framework;
using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public class BasicTests
    {
        [Test]
        public void Can_connect_to_redis_server()
        {
            var client = ConnectionMultiplexer.Connect(Settings.Default.RedisServerUrl);
            Assert.True(client.IsConnected);
        }

        [Test]
        public void Throws_if_trying_to_connect_to_bad_server()
        {
            Assert.Throws<RedisConnectionException>(() => ConnectionMultiplexer.Connect("localhost:80"));
        }

        [Test]
        public void Can_get_db_once_connected()
        {
            var client = ConnectionMultiplexer.Connect(Settings.Default.RedisServerUrl);
            var db = client.GetDatabase();
            Assert.True(db.IsConnected("key"));
        }

        [Test]
        public void Can_flush_db_in_admin_mode()
        {
            var client = ConnectionMultiplexer.Connect(Settings.Default.RedisServerUrl+",allowAdmin=true");
            client.GetServer(Settings.Default.RedisServerUrl).FlushAllDatabases();
        }
    }
}
