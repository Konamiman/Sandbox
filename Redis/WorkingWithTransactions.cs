﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithTransactions : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_execute_basic_transaction()
        {
            var transaction = db.CreateTransaction();

            var response1 = transaction.StringSetAsync("foo", "bar");
            var response2 = transaction.StringSetAsync("fizz", "buzz");

            transaction.Execute();

            Task.WaitAll(new[] { response1, response2 }, 2000);

            Assert.AreEqual("bar", (string)db.StringGet("foo"));
            Assert.AreEqual("buzz", (string)db.StringGet("fizz"));
        }

        [Test]
        public void Conditional_transactions_do_not_execute_if_condition_is_not_met()
        {
            var transaction = db.CreateTransaction();
            transaction.AddCondition(Condition.KeyExists("foo"));
            
            var response1 = transaction.StringSetAsync("a", 1);

            transaction.Execute();

            try
            {
                response1.Wait(5000);
            }
            catch(AggregateException ex)
            {
                ex.Handle((e) => true);
            }

            Assert.False(db.KeyExists("a"));
        }

        [Test]
        public void Conditional_transactions_do_not_execute_if_only_some_conditions_are_met()
        {
            db.StringSet("foo", "bar");

            var transaction = db.CreateTransaction();
            transaction.AddCondition(Condition.KeyExists("foo"));
            transaction.AddCondition(Condition.StringEqual("fizz", "buzz"));

            var response1 = transaction.StringSetAsync("a", 1);

            transaction.Execute();

            try
            {
                response1.Wait(5000);
            }
            catch (AggregateException ex)
            {
                ex.Handle((e) => true);
            }

            Assert.False(db.KeyExists("a"));
        }

        [Test]
        public void Conditional_transactions_executes_if_all_conditions_are_met()
        {
            db.StringSet("foo", "bar");
            db.StringSet("fizz", "buzz");

            var transaction = db.CreateTransaction();
            transaction.AddCondition(Condition.KeyExists("foo"));
            transaction.AddCondition(Condition.StringEqual("fizz", "buzz"));

            var response1 = transaction.StringSetAsync("a", 1);

            transaction.Execute();
            response1.Wait(2000);

            Assert.AreEqual(1, (int)db.StringGet("a"));
        }

        [Test]
        public void Transaction_operations_are_not_performed_if_transaction_is_not_executed()
        {
            var transaction = db.CreateTransaction();

            var response1 = transaction.StringSetAsync("foo", "bar");
            var response2 = transaction.StringSetAsync("fizz", "buzz");

            Assert.False(db.KeyExists("foo"));
            Assert.False(db.KeyExists("fizz"));
        }
    }
}
