﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithSortedSets : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_add_items_to_sorted_set_and_retrieve_them()
        {
            db.SortedSetAdd("foo", "bar", 1);
            db.SortedSetAdd("foo", "fizz", 2);
            db.SortedSetAdd("foo", "MSX", 3);

            var items = db.SortedSetScan("foo").OrderByDescending(i => i.Score).ToArray();

            Assert.AreEqual(3, items.Length);
            Assert.AreEqual(3, items[0].Score);
            Assert.AreEqual("MSX", (string)items[0].Element);
            Assert.AreEqual(2, items[1].Score);
            Assert.AreEqual("fizz", (string)items[1].Element);
            Assert.AreEqual(1, items[2].Score);
            Assert.AreEqual("bar", (string)items[2].Element);
        }

        [Test]
        public void Can_get_rank_of_stored_items()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            var rankAscending = db.SortedSetRank("foo", "MSX", Order.Ascending);
            var rankDescending = db.SortedSetRank("foo", "MSX", Order.Descending);

            Assert.AreEqual(2, rankAscending);
            Assert.AreEqual(1, rankDescending);
        }

        [Test]
        public void Can_combine_sets_using_aggregation_for_the_scores()
        {
            db.SortedSetAdd("foo1", "bar", 10);
            db.SortedSetAdd("foo1", "fizz", 20);
            db.SortedSetAdd("foo1", "MSX", 30);

            db.SortedSetAdd("foo2", "Twingo", 40);
            db.SortedSetAdd("foo2", "MSX", 50);

            db.SortedSetCombineAndStore(SetOperation.Union, "foo3", "foo1", "foo2", Aggregate.Sum);

            var items = db.SortedSetScan("foo3").OrderBy(i => i.Score).ToArray();

            Assert.AreEqual(4, items.Length);
            Assert.AreEqual(10, items[0].Score);
            Assert.AreEqual("bar", (string)items[0].Element);
            Assert.AreEqual(20, items[1].Score);
            Assert.AreEqual("fizz", (string)items[1].Element);
            Assert.AreEqual(40, items[2].Score);
            Assert.AreEqual("Twingo", (string)items[2].Element);
            Assert.AreEqual(80, items[3].Score);
            Assert.AreEqual("MSX", (string)items[3].Element);
        }

        [Test]
        public void Can_increment_score_of_item()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetIncrement("foo", "bar", 5);

            var item = db.SortedSetScan("foo").Single();

            Assert.AreEqual(15, item.Score);
        }

        [Test]
        public void Can_get_count_of_items_with_score_in_certain_range()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            var count = db.SortedSetLength("foo", 20, 30, Exclude.None);

            Assert.AreEqual(2, count);
        }

        [Test]
        public void Can_get_count_of_items_with_value_in_certain_range()
        {
            db.SortedSetAdd("foo", 10, 1);
            db.SortedSetAdd("foo", 20, 1);
            db.SortedSetAdd("foo", 30, 1);
            db.SortedSetAdd("foo", 40, 1);

            var count = db.SortedSetLengthByValue("foo", 20, 30, Exclude.None);

            Assert.AreEqual(2, count);
        }

        [Test]
        public void Can_get_items_with_rank_in_certain_range()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            var items = db.SortedSetRangeByRank("foo", 1, 2, Order.Ascending).ToArray();

            Assert.AreEqual(2, items.Length);
            Assert.AreEqual("fizz", (string)items[0]);
            Assert.AreEqual("MSX", (string)items[1]);
        }

        [Test]
        public void Can_get_items_and_scores_with_rank_in_certain_range()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            var items = db.SortedSetRangeByRankWithScores("foo", 1, 2, Order.Ascending).ToArray();

            Assert.AreEqual(2, items.Length);
            Assert.AreEqual(20, items[0].Score);
            Assert.AreEqual("fizz", (string)items[0].Element);
            Assert.AreEqual(30, items[1].Score);
            Assert.AreEqual("MSX", (string)items[1].Element);
        }

        [Test]
        public void Can_remove_item_by_value()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            db.SortedSetRemove("foo", "fizz");

            var items = db.SortedSetScan("foo").Select(i => (string)i.Element).ToArray();

            CollectionAssert.AreEquivalent(new[] {"bar", "MSX", "Twingo"}, items);
        }

        [Test]
        public void Can_remove_items_by_rank()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            db.SortedSetRemoveRangeByRank("foo", 1, 2);

            var items = db.SortedSetScan("foo").Select(i => (string)i.Element).ToArray();

            CollectionAssert.AreEquivalent(new[] { "bar", "Twingo" }, items);
        }

        [Test]
        public void Can_remove_items_by_score()
        {
            db.SortedSetAdd("foo", "bar", 10);
            db.SortedSetAdd("foo", "fizz", 20);
            db.SortedSetAdd("foo", "MSX", 30);
            db.SortedSetAdd("foo", "Twingo", 40);

            db.SortedSetRemoveRangeByScore("foo", 20, 30, Exclude.None);

            var items = db.SortedSetScan("foo").Select(i => (string)i.Element).ToArray();

            CollectionAssert.AreEquivalent(new[] { "bar", "Twingo" }, items);
        }

        [Test]
        public void Can_remove_items_by_value()
        {
            db.SortedSetAdd("foo", 10, 1);
            db.SortedSetAdd("foo", 20, 1);
            db.SortedSetAdd("foo", 30, 1);
            db.SortedSetAdd("foo", 40, 1);

            db.SortedSetRemoveRangeByValue("foo", 20, 30, Exclude.None);

            var items = db.SortedSetScan("foo").Select(i => (int)i.Element).ToArray();

            CollectionAssert.AreEquivalent(new[] { 10, 40 }, items);
        }
    }
}
