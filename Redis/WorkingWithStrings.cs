﻿using NUnit.Framework;
using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithStrings : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_add_string_values_by_string_key()
        {
            var value = db.StringGet("foo");
            Assert.False(value.HasValue);

            db.StringSet("foo", "bar");

            value = db.StringGet("foo");
            Assert.True(value.HasValue);
            Assert.AreEqual("bar", (string)value);
        }

        [Test]
        public void Can_add_binary_values_by_binary_key()
        {
            var key = new byte[] {1, 2, 3};
            var value = new byte[] { 4, 5, 6, 7 };

            var retrievedValue = db.StringGet(key);
            Assert.False(retrievedValue.HasValue);
            db.StringSet(key, value);

            retrievedValue = db.StringGet(key);
            Assert.True(retrievedValue.HasValue);
            CollectionAssert.AreEqual(value, (byte[])retrievedValue);
        }

        [Test]
        public void Can_append_to_string_values()
        {
            db.StringSet("foo", "bar");
            db.StringAppend("foo", "buzz");

            var value = db.StringGet("foo");
            Assert.AreEqual("barbuzz", (string)value);
        }

        [Test]
        public void Can_append_to_binary_values()
        {
            db.StringSet("foo", new byte[] {1, 2, 3});
            db.StringAppend("foo", new byte[] {4, 5, 6});

            var value = db.StringGet("foo");
            CollectionAssert.AreEqual(new byte[] {1, 2, 3, 4, 5, 6}, (byte[])value);
        }

        [Test]
        public void Can_count_number_of_bits_in_value()
        {
            db.StringSet("foo", new byte[] {1, 128, 255});
            
            var bitCount = db.StringBitCount("foo");
            Assert.AreEqual(10, bitCount);
        }

        [Test]
        public void Can_perform_bit_operation_between_two_keys_considering_string_values()
        {
            db.StringSet("foo", "ABCDE");
            db.StringSet("bar", "     ");

            db.StringBitOperation(Bitwise.Or, "result", "foo", "bar");

            var result = db.StringGet("result");
            Assert.AreEqual("abcde", (string)result);
        }

        [Test]
        public void Can_find_first_set_or_reset_bit()
        {
            db.StringSet("foo", new byte[] {0, 0, 0, 128});

            var position = db.StringBitPosition("foo", bit: true);
            
            Assert.AreEqual(24, position);
        }

        [Test]
        public void Can_set_and_reset_individual_bits()
        {
            db.StringSet("foo", new byte[] { 127 });

            db.StringSetBit("foo", 0, true);  //MSB
            db.StringSetBit("foo", 7, false); //LSB

            var value = db.StringGet("foo");
            Assert.AreEqual(254, ((byte[])value)[0]);
        }

        [Test]
        public void Can_increment_stored_value()
        {
            db.StringSet("foo", 42);

            db.StringIncrement("foo");

            var value = db.StringGet("foo");
            Assert.AreEqual(43, (int)value);
        }

        [Test]
        public void Can_get_range_of_stored_value()
        {
            db.StringSet("foo", "fizzbuzz");

            var value = db.StringGetRange("foo", start: 1, end: 4);

            Assert.AreEqual("izzb", (string)value);
        }

        [Test]
        public void Can_set_range_of_stored_value()
        {
            db.StringSet("foo", "fizzbuzz");

            db.StringSetRange("foo", 1, "bar");
            
            var value = db.StringGet("foo");
            Assert.AreEqual("fbarbuzz", (string)value);
        }

        [Test]
        public void Can_get_length_of_stored_value()
        {
            db.StringSet("foo", "fizzbuzz");

            var length = db.StringLength("foo");
            Assert.AreEqual(8, length);
        }

        [Test]
        public void Can_atomically_change_value_and_return_old_value()
        {
            db.StringSet("foo", "bar");
            
            var oldValue = db.StringGetSet("foo", "fizzbuzz");
            var newValue = db.StringGet("foo");

            Assert.AreEqual("bar", (string)oldValue);
            Assert.AreEqual("fizzbuzz", (string)newValue);
        }
    }
}
