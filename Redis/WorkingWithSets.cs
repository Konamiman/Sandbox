﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithSets : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_add_members()
        {
            db.SetAdd("foo", "bar");
            db.SetAdd("foo", "fizz");

            var members = OrderedStrings(db.SetMembers("foo"));

            Assert.AreEqual(2, members.Length);
            Assert.AreEqual("bar", (string)members[0]);
            Assert.AreEqual("fizz", (string)members[1]);
        }

        [Test]
        public void Can_calculate_combinations_on_sets()
        {
            db.SetAdd("foo1", "bar");
            db.SetAdd("foo1", "fizz");
            db.SetAdd("foo1", "MSX");
            db.SetAdd("foo1", "Twingo");

            db.SetAdd("foo2", "MSX");
            db.SetAdd("foo2", "Twingo");
            db.SetAdd("foo2", "buzz");
            db.SetAdd("foo2", "pizza");

            var union = OrderedStrings(db.SetCombine(SetOperation.Union, "foo1", "foo2"));
            CollectionAssert.AreEquivalent(new[] {"bar", "fizz", "MSX", "Twingo", "buzz", "pizza"}, union);

            var intersection = OrderedStrings(db.SetCombine(SetOperation.Intersect, "foo1", "foo2"));
            CollectionAssert.AreEquivalent(new[] { "MSX", "Twingo" }, intersection);

            var difference = OrderedStrings(db.SetCombine(SetOperation.Difference, "foo1", "foo2"));
            CollectionAssert.AreEquivalent(new[] { "bar", "fizz" }, difference);
        }

        [Test]
        public void Can_check_if_value_exists_in_set()
        {
            db.SetAdd("foo", "bar");
            db.SetAdd("foo", "fizz");

            Assert.IsTrue(db.SetContains("foo", "bar"));
            Assert.IsFalse(db.SetContains("foo", "XXX"));
        }

        [Test]
        public void Can_move_value_from_one_set_to_another()
        {
            db.SetAdd("foo1", "bar");
            db.SetAdd("foo1", "fizz");
            db.SetAdd("foo2", "MSX");
            db.SetAdd("foo2", "Twingo");

            db.SetMove("foo1", "foo2", "fizz");

            CollectionAssert.AreEquivalent(new[] { "bar" }, OrderedStrings(db.SetMembers("foo1")));
            CollectionAssert.AreEquivalent(new[] { "fizz", "MSX", "Twingo" }, OrderedStrings(db.SetMembers("foo2")));
        }

        [Test]
        public void Can_get_one_random_value_from_the_set()
        {
            db.SetAdd("foo", "bar");
            db.SetAdd("foo", "fizz");
            db.SetAdd("foo", "MSX");
            db.SetAdd("foo", "Twingo");

            var value = db.SetPop("foo");

            CollectionAssert.Contains(new[] { "bar", "fizz", "MSX", "Twingo" }, (string)value);
        }

        [Test]
        public void Can_retrieve_a_random_subset_of_items()
        {
            db.SetAdd("foo", "bar");
            db.SetAdd("foo", "fizz");
            db.SetAdd("foo", "MSX");
            db.SetAdd("foo", "Twingo");

            var values = OrderedStrings(db.SetRandomMembers("foo", 2));

            CollectionAssert.IsSubsetOf(values, new[] { "bar", "fizz", "MSX", "Twingo" });
        }

        [Test]
        public void Can_remove_an_item_from_the_set()
        {
            db.SetAdd("foo", "bar");
            db.SetAdd("foo", "fizz");
            db.SetAdd("foo", "MSX");
            db.SetAdd("foo", "Twingo");

            db.SetRemove("foo", "MSX");

            CollectionAssert.AreEquivalent(new[] { "bar", "fizz", "Twingo" }, OrderedStrings(db.SetMembers("foo")));
        }

        [Test]
        public void Can_scan_set_items_as_IEnumerable()
        {
            var items = new List<string>();

            db.SetAdd("foo", "bar");
            db.SetAdd("foo", "fizz");
            db.SetAdd("foo", "MSX");
            db.SetAdd("foo", "Twingo");

            foreach(var item in db.SetScan("foo"))
                items.Add((string)item);

            CollectionAssert.AreEquivalent(new[] { "bar", "fizz", "MSX", "Twingo" }, items);
        }

        private string[] OrderedStrings(RedisValue[] values)
        {
            return values.Select(v => (string)v).OrderBy(v => v).ToArray();
        }
    }
}
