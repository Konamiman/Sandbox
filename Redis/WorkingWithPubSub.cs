﻿using System.Threading;
using NUnit.Framework;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithPubSub : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_publish_messages_that_are_received_by_subscribers()
        {
            var subscriber = client.GetSubscriber();
            string channel = null;
            string value = null;

            var ev = new ManualResetEvent(false);

            subscriber.Subscribe(
                "channel",
                (ch, val) => { channel = (string)ch; value = (string)val; ev.Set(); });
            subscriber.Publish("channel", "foobar");
            
            var signaled = ev.WaitOne(2000);

            Assert.True(signaled);
            Assert.AreEqual("channel", channel);
            Assert.AreEqual("foobar", value);
        }
    }
}
