﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithHashes : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_set_and_get_hash_fields_individually()
        {
            db.HashSet("foo", "fizz", "buzz");
            db.HashSet("foo", "answer", 42);

            var value = db.HashGet("foo", "fizz");
            Assert.AreEqual("buzz", (string)value);

            value = db.HashGet("foo", "answer");
            Assert.AreEqual(42, (int)value);
        }

        [Test]
        public void Can_set_and_get_hash_fields_collectively()
        {
            db.HashSet("foo", new HashEntry[] {new HashEntry("fizz", "buzz"), new HashEntry("answer", 42)});

            var value = db.HashGetAll("foo");

            Assert.AreEqual(2, value.Length);
            var valuesDictionary = value.ToDictionary();
            Assert.AreEqual("buzz", (string)valuesDictionary["fizz"]);
            Assert.AreEqual(42, (int)valuesDictionary["answer"]);
        }

        [Test]
        public void Can_check_if_specific_hash_field_exists()
        {
            db.HashSet("foo", "fizz", "buzz");
            Assert.True(db.HashExists("foo", "fizz"));
            Assert.False(db.HashExists("foo", "bad_field"));
            Assert.False(db.HashExists("bad_key", "bad_field"));
        }

        [Test]
        public void Can_delete_fields_from_hash()
        {
            db.HashSet("foo", new HashEntry[] { new HashEntry("fizz", "buzz"), new HashEntry("answer", 42) });
            db.HashDelete("foo", "fizz");

            var value = db.HashGetAll("foo");

            Assert.AreEqual(1, value.Length);
            var valuesDictionary = value.ToDictionary();
            Assert.AreEqual(42, (int)valuesDictionary["answer"]);

            db.HashDelete("foo", "bad_field");
            db.HashDelete("bad_key", "bad_field");
        }

        [Test]
        public void Can_increment_hash_field_values()
        {
            db.HashSet("foo", new HashEntry[] { new HashEntry("fizz", "buzz"), new HashEntry("answer", 42) });
            db.HashIncrement("foo", "answer", 10);

            var value = db.HashGetAll("foo");

            var valuesDictionary = value.ToDictionary();
            Assert.AreEqual(52, (int)valuesDictionary["answer"]);
        }

        [Test]
        public void Can_retrieve_hash_keys()
        {
            db.HashSet("foo", new HashEntry[] { new HashEntry("fizz", "buzz"), new HashEntry("answer", 42) });

            var keys = db.HashKeys("foo");

            Assert.AreEqual(2, keys.Length);
            Assert.AreEqual("fizz", (string)keys[0]);
            Assert.AreEqual("answer", (string)keys[1]);
        }

        [Test]
        public void Can_retrieve_hash_values()
        {
            db.HashSet("foo", new HashEntry[] { new HashEntry("fizz", "buzz"), new HashEntry("answer", 42) });

            var values = db.HashValues("foo");

            Assert.AreEqual(2, values.Length);
            Assert.AreEqual("buzz", (string)values[0]);
            Assert.AreEqual(42, (int)values[1]);
        }

        [Test]
        public void Can_scan_hash_fields_as_IEnumerable()
        {
            var keysAndValues = new Dictionary<RedisValue, RedisValue>();

            db.HashSet("foo", new HashEntry[] { new HashEntry("fizz", "buzz"), new HashEntry("answer", 42) });

            foreach(var hashItem in db.HashScan("foo"))
            {
                keysAndValues[hashItem.Name] = hashItem.Value;
            }

            Assert.AreEqual(2, keysAndValues.Count);
            Assert.AreEqual("buzz", (string)keysAndValues["fizz"]);
            Assert.AreEqual(42, (int)keysAndValues["answer"]);
        }

        [Test]
        public void Can_delete_hash_with_KeyDelete()
        {
            db.HashSet("foo", new HashEntry[] { new HashEntry("fizz", "buzz"), new HashEntry("answer", 42) });
            Assert.IsTrue(db.KeyExists("foo"));

            db.KeyDelete("foo");
            Assert.IsFalse(db.KeyExists("foo"));
        }
    }
}
