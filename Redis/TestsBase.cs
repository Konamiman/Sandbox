﻿using StackExchange.Redis;

namespace Konamiman.Sandbox.Redis
{
    public abstract class TestsBase
    {
        protected ConnectionMultiplexer client;
        protected IDatabase db;

        protected void BaseSetup()
        {
            client = ConnectionMultiplexer.Connect(Settings.Default.RedisServerUrl + ",allowAdmin=true");
            client.GetServer(Settings.Default.RedisServerUrl).FlushAllDatabases();
            db = client.GetDatabase();
        }
    }
}
