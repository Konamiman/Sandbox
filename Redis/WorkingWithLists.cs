﻿using NUnit.Framework;

namespace Konamiman.Sandbox.Redis
{
    public class WorkingWithLists : TestsBase
    {
        [SetUp]
        public void Setup()
        {
            base.BaseSetup();
        }

        [Test]
        public void Can_add_items_to_head_or_tail_of_list()
        {
            db.ListLeftPush("foo", "bar");
            db.ListLeftPush("foo", 42);
            db.ListRightPush("foo", "fizz");

            var values = db.ListRange("foo");

            Assert.AreEqual(3, values.Length);
            Assert.AreEqual(42, (int)values[0]);
            Assert.AreEqual("bar", (string)values[1]);
            Assert.AreEqual("fizz", (string)values[2]);
        }

        [Test]
        public void Can_add_item_after_certain_item()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");

            db.ListInsertAfter("foo", "fizz", 42);

            var values = db.ListRange("foo");

            Assert.AreEqual(4, values.Length);
            Assert.AreEqual("bar", (string)values[0]);
            Assert.AreEqual("fizz", (string)values[1]);
            Assert.AreEqual(42, (int)values[2]);
            Assert.AreEqual("buzz", (string)values[3]);
        }

        [Test]
        public void Can_pop_item_from_head_or_tail_of_list()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");

            var leftValue = db.ListLeftPop("foo");
            var rightValue = db.ListRightPop("foo");
            var remainingValues = db.ListRange("foo");

            Assert.AreEqual("bar", (string)leftValue);
            Assert.AreEqual("buzz", (string)rightValue);
            Assert.AreEqual(1, remainingValues.Length);
            Assert.AreEqual("fizz", (string)remainingValues[0]);
        }

        [Test]
        public void Can_get_range_of_items_by_index()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");
            db.ListRightPush("foo", "Twingo");
            db.ListRightPush("foo", "MSX");

            var values = db.ListRange("foo", 1, -2);

            Assert.AreEqual(3, values.Length);
            Assert.AreEqual("fizz", (string)values[0]);
            Assert.AreEqual("buzz", (string)values[1]);
            Assert.AreEqual("Twingo", (string)values[2]);
        }

        [Test]
        public void Can_retrieve_item_by_index()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");

            var item = db.ListGetByIndex("foo", 1);

            Assert.AreEqual("fizz", (string)item);
        }

        [Test]
        public void Can_get_list_length()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");

            var length = db.ListLength("foo");

            Assert.AreEqual(3, length);
        }

        [Test]
        public void Can_remove_item_by_value()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");

            db.ListRemove("foo", "fizz");

            var values = db.ListRange("foo");

            Assert.AreEqual(2, values.Length);
            Assert.AreEqual("bar", (string)values[0]);
            Assert.AreEqual("buzz", (string)values[1]);
        }

        [Test]
        public void Can_atomically_move_item_from_tail_of_list_to_head_of_other_list()
        {
            db.ListRightPush("foo1", "bar");
            db.ListRightPush("foo1", "fizz");
            db.ListRightPush("foo1", "buzz");

            db.ListRightPush("foo2", 34);
            db.ListRightPush("foo2", 42);
            db.ListRightPush("foo2", 89);

            db.ListRightPopLeftPush("foo1", "foo2");

            var values1 = db.ListRange("foo1");
            var values2 = db.ListRange("foo2");

            Assert.AreEqual(2, values1.Length);
            Assert.AreEqual("bar", (string)values1[0]);
            Assert.AreEqual("fizz", (string)values1[1]);
            Assert.AreEqual(4, values2.Length);
            Assert.AreEqual("buzz", (string)values2[0]);
            Assert.AreEqual(34, (int)values2[1]);
        }

        [Test]
        public void Can_remove_occurences_of_certain_item()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "Twingo");
            db.ListRightPush("foo", "bar");

            db.ListRemove("foo", "bar", 0);

            var values = db.ListRange("foo");

            Assert.AreEqual(2, values.Length);
            Assert.AreEqual("fizz", (string)values[0]);
            Assert.AreEqual("Twingo", (string)values[1]);
        }

        [Test]
        public void Can_trim_list_to_contain_only_range_of_items()
        {
            db.ListRightPush("foo", "bar");
            db.ListRightPush("foo", "fizz");
            db.ListRightPush("foo", "buzz");
            db.ListRightPush("foo", "Twingo");
            db.ListRightPush("foo", "MSX");

            db.ListTrim("foo", 1, -2);

            var values = db.ListRange("foo");

            Assert.AreEqual(3, values.Length);
            Assert.AreEqual("fizz", (string)values[0]);
            Assert.AreEqual("buzz", (string)values[1]);
            Assert.AreEqual("Twingo", (string)values[2]);
        }
    }
}
